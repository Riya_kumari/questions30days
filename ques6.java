package cwcquestion30days;
import java.util.*;
public class ques6 {
static int swapBits(int n, int a1, int a2, int r)
		    {
		        int set1 = (n >> a1) & ((1 << r) - 1);	 
		        int set2 = (n >> a2) & ((1 << r) - 1);
		        int xor = (set1 ^ set2);
		        xor = (xor << a1) | (xor << a2);
		        int result = n ^ xor;
		        return result;
		    }
public static void main(String[] args)
		    {
	int res = swapBits(10, 0, 3, 2);
		      System.out.println("Result = " + res);	
		}
	}
