package cwcquestion30days;
import java.math.BigInteger;
public class ques95 {

	public static void main(String[] args) {
		BigInteger sum = new BigInteger("2");
        for(int i=3; i<10; i++){
            for (int j=2; j<i; j++){
                if (i % j == 0) 
                    break;
                else if (i == j+1){
                    sum = sum.add(BigInteger.valueOf(i));
                }
            }
        }
        System.out.println("Sum  = "+sum); 
    }
}