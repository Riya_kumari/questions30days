package cwcquestion30days;
import java.io.*;
import java.util.*;
public class ques80 {
public static int SIZE = 8;
		    static int bit_anagram_check(long a, long b)
		    {
		            int i = 0;
		        long[] binary_a = new long[SIZE];
		        Arrays.fill(binary_a, 0);
		        while (a > 0)
		        {
		            binary_a[i] = a%2;
		            a /= 2;
		            i++;
		        }
		        int j = 0;
		        long[] binary_b = new long[SIZE];
		        Arrays.fill(binary_b, 0);
		        while (b > 0)
		        {
		            binary_b[j] = b%2;
		            b /= 2;
		            j++;
		        }
		         Arrays.sort(binary_a);
		        Arrays.sort(binary_b);
		       for (i = 0; i < SIZE; i++)
		            if (binary_a[i] != binary_b[i])
		                return 0;
		         return 1;
		    }
		    public static void main (String[] args)
		    {
		        long a = 8, b = 4;
		        System.out.println(bit_anagram_check(a, b));
		    }
		}