package cwcquestion30days;
import java.io.*;
public class ques75 {
		  static long doublefactorial(long n)
		    {
		        if (n == 0 || n==1)
		            return 1;
		             
		        return n * doublefactorial(n - 2);
		    }
		    static public void main (String[] args)
		    {
		        System.out.println("Double factorial"
		            + " is " + doublefactorial(5));
		    }
}