package cwcquestion30days;
import java.util.*;
public class ques70 {
public static int findMinSum(int num)
{
int sum = 0; 
for (int i = 2; i * i <= num; i++) { 
while (num % i == 0) { 
sum += i; 
num /= i; 
}
} 
sum += num; 
return sum;    
}
public static void main(String[]args) {
	Scanner sc =new Scanner(System.in);
	int num=sc.nextInt();
System.out.println(findMinSum(num));
}
}