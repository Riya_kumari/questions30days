package cwcquestion30days;

public class ques10 {
	
		static int Triplets(int n)
		{
		    
		    int ans = 0;
		    for (int i = 1; i <= n; ++i) {
		        for (int j = i; j <= n; ++j) {
		            int x = i * i + j * j;
		 
		            
		            int y =(int) Math.sqrt(x);
		            if (y * y == x && y <= n)
		                ++ans;
		        }
		    }
		  return ans;
		}
		public static void main(String args[])
		{
		    int n = 10;
		    System.out.println(Triplets(n));
		}
		}