package cwcquestion30days;
import java.util.Arrays;
import java.util.*;
public class ques33 {
public static void main(String[] args) {
		      Scanner sc = new Scanner(System.in);
		      int t = sc.nextInt();
		      while(t-- >0)
		      {
		         int n = sc.nextInt();
		          int arr[] = new int[n];
		          for(int i = 0;i<n;i++){
		              arr[i] =sc.nextInt();
		          }
		          Arrays.sort(arr);
		          for(int i :arr)
		          System.out.print(i+" ");  
		      }   
		    }
}
