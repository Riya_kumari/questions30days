package cwcquestion30days;
import java.util.*;
public class ques27 {
	    public static int first(int[] nums) {
	        for (int index = 0; index < nums.length; index++) {
	            while(nums[index] > 0 && nums[index] < nums.length){
	                if (index+1 == nums[index]) break;
	                if(nums[index] == nums[nums[index] -1]) break;
	                int temp = nums[index];
	                nums[index] = nums[temp-1];
	                nums[temp-1] = temp;
	            }    
	        }
	        for (int index = 0; index <= nums.length; index++) {
	            if(index == nums.length) return nums.length+1;
	            if(nums[index] != index+1) {
	                return index+1;
	            }   
	        }
	        return nums.length;
	    }
	    public static void main(String []args){
	        Scanner sc =new Scanner(System.in);
	        int n =sc.nextInt();
	        int num[]=new int[n];
	        for(int i =0;i<n;i++){
	            num[i]=sc.nextInt();
	        }
	        int r = first(num);
	        System.out.print(r);
	    }
	}