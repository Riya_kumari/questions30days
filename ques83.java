package cwcquestion30days;
import java.util.Scanner;
public class ques83 {

	public static void main(String[] args) {
		Scanner sr = new Scanner(System.in);
        int n = sr.nextInt();
        int a[] = new int[n];
        for(int i=0;i<n;i++)
        {
            a[i] = sr.nextInt();
        }
        System.out.println("Leaders are : ");  
        int cur_max = -1;
        for(int i=n-1;i>=0;i--)
        {
          if(a[i]>cur_max)
          {
              System.out.print(a[i]+" ");
              cur_max = a[i];
          }
        }
}
}