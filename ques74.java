package cwcquestion30days;
import java.io.*;
public class ques74 {
		  static float harmonicMean(float arr[], int n)
		    {
		        float sum = 0;
		        for (int i = 0; i < n; i++)
		            sum = sum + (float)1 / arr[i];
		      
		        return (float)n/sum;
		    }
		    public static void main(String args[])
		    {
		        float arr[]= { 13.5f, 14.5f, 14.8f,
		                      15.2f, 16.1f };
		        int n = arr.length;
		        System.out.println(harmonicMean(arr, n));
		    }

	}