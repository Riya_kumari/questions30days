package cwcquestion30days;
import java.io.*;
public class ques30 {
static void segregateElements(int arr[], int p) 
		    {
	int temp[] = new int[p]; 
    int j = 0;  
    for (int i = 0; i < p; i++) 
    if (arr[i] >= 0) 
	temp[j++] = arr[i]; 
    if (j == p || j == 0) 
    return; 
	for (int i = 0; i < p; i++) 
    if (arr[i] < 0) 
	temp[j++] = arr[i]; 
	for (int i = 0; i < p; i++) 
    arr[i] = temp[i]; 
		    } 
 public static void main(String arg[]) 
		    { 
		        int arr[] = { -3, 22, -45 , 67, 8,10, 9, -2, -5}; 
		        int p = arr.length; 
		        segregateElements(arr, p); 
		 for (int i = 0; i < p; i++) 
		            System.out.print(arr[i] + " "); 
}
}